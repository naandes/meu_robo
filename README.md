

## Meu Robô Nands

Por ser uma lógica nova, tentei fazer mudanças que deixassem o meu robô com uma lógica mais complexa. Entretanto, percebi que estava perdendo para os robôs que já tinham no programa. Assim sendo, programei um robô que ficasse atirando enquanto fazia um giro de 360 graus, para evitar que batesse na parede caso fosse reto. Nesse sentido, fugiria contantemente do alvo que outros robôs que travassem a mira no meu robô e na maior parte do tempo não se chocaria na parede. dessa forma, meu robô contra todos os pré programados do próprio programa ou ganhava com mais de 56% em todas as vezes. 

O ponto forte do meu robô é a alta mobilidade dele e o maior ponto fraco é a mira que não tem nenhuma sofisticação para procura do adversário. 

O que eu mais gostei da construção da Lógica do meu Robô foi as tentativas de melhora-lo, pesquisar formas e lógicas novas e mesmo tendo mais armas, acredito que no momento eu tive que escolher pelo mais simples pra ter uma performace melhor.
